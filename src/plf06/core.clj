(ns plf06.core
  (:gen-class))

(defn caracter-rot13 [caracter] (let [mas13 (find (assoc {} \a "k" \á "l" \b "m" \c "n" \d "ñ" \e "o" 
\é "ó" \f "p" \g "q" \h "r" \i "s" \í "t" \j "u" \k "ú" \l "ü" \m "v" \n "w" \ñ "x" \o "y" \ó "z"
\p "a" \q "á" \r "b" \s "c" \t "d" \u "e" \ú "é" \ü "f" \v "g" \w "h" \x "i" \y "í" \z "j"
\A "K" \Á "L" \B "M" \C "N" \D "Ñ" \E "O" \É "Ó" \F "P" \G "Q" \H "R" \I "S" \Í "T" \J "U" 
\K "Ú" \L "Ü" \M "V" \N "W" \Ñ "X" \O "Y" \Ó "Z" \P "A" \Q "Á" \R "B" \S "C" \T "D" \U "E" 
\Ú "É" \Ü "F" \V "G" \W "H" \X "I" \Y "Í" \Z "J" \0 ")" \1 "*" \2 "+" \3 "," \4 "-" \! "." 
\¨ "/" \# ":" \$ ";" \% "<" \& "=" \' ">" \* "[" \+ "ª" \- "^" \. "_" \/ "`" \< "}" \= "~" 
\> "5" \? "6" \ª "9" \_ "2" \| "3" \5 "$" \6 "%" \7 "&" \8 "'" \9 "(" \] "0" 
\^ "1" \` "3" \{ "4" \| "!" \} "¨" \( "?" \) "@" \, "]" \: "{" \; "|" \@ "7" \[ "8" \~ "#") 
caracter)] (if (= mas13 nil) caracter (val mas13))))

(defn cifrado-rot13 [palabra] (let [cifrado (map caracter-rot13 palabra)]
                                  (apply str cifrado)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
(if (empty? args)
  (println "Error. Porfavor introduce una o varias cadenas de caracteres") 
  (println (cifrado-rot13 (apply str args))))) 